#include <linux/i2c-dev.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <errno.h>

#include "lsm303.h"


bool read_CTRL_REG_1A(int *fd, uint8_t *read_buffer, size_t buffer_size)
{
    bool read_success = false;
    uint8_t reg = 0x20;

    uint8_t outbuf[1];
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = 0x19;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = 0x19;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;

    outbuf[0] = 0x20;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool write_CTRL_REG_1A(int *fd, uint8_t data)
{

    bool read_success = false;
    uint8_t reg = 0x20;

    int retval;
    uint8_t outbuf[2];
    outbuf[0] = reg;
    outbuf[1] = data;
    struct i2c_msg msgs[3];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = 0x19;
    msgs[0].flags = 0;
    msgs[0].len = 2;
    msgs[0].buf = outbuf;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 1;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c write data\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool get_accelerometer_x_axis_accel(int *fd, int16_t *read_data)
{
    bool read_success = false;
    *read_data = 0;

    uint8_t outbuf[1];
    uint8_t read_buffer[1];
    uint16_t raw_data = 0;
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = 0x19;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = 0x19;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;

    outbuf[0] = 0x28;


        int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
        {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            raw_data |= read_buffer[0];

            msgs[0].addr = 0x19;
            msgs[0].flags = 0;
            msgs[0].len = 1;
            msgs[0].buf = outbuf;

            msgs[1].addr = 0x19;
            msgs[1].flags = I2C_M_RD;
            msgs[1].len = 1;
            msgs[1].buf = read_buffer;

            msgset[0].msgs = msgs;
            msgset[0].nmsgs = 2;

            outbuf[0] = 0x29;
            
            if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
            {
                perror("ioctl(I2C_RDWR) in i2c_read\n");
            } 
            else
            {
                raw_data |= (read_buffer[0] << 8);
                if (raw_data & (1 << 15))
                {
                    raw_data = ~raw_data;
                    raw_data >>= 4;
                    raw_data += 1;
                    raw_data = -raw_data;
                     
                } 
                else 
                {
                    raw_data >>= 4;
                }
                *read_data |= raw_data;
                read_success = true;
            }
            
        }

    return read_success;

}

bool get_accelerometer_y_axis_accel(int *fd, int16_t *read_data)
{
    bool read_success = false;
    *read_data = 0;

    uint8_t outbuf[1];
    uint8_t read_buffer[1];
    uint16_t raw_data = 0;
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = 0x19;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = 0x19;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;

    outbuf[0] = 0x2A;


        int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
        {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            raw_data = read_buffer[0];

            msgs[0].addr = 0x19;
            msgs[0].flags = 0;
            msgs[0].len = 1;
            msgs[0].buf = outbuf;

            msgs[1].addr = 0x19;
            msgs[1].flags = I2C_M_RD;
            msgs[1].len = 1;
            msgs[1].buf = read_buffer;

            msgset[0].msgs = msgs;
            msgset[0].nmsgs = 2;

            outbuf[0] = 0x2B;
            
            if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
            {
                perror("ioctl(I2C_RDWR) in i2c_read\n");
            } 
            else
            {
                raw_data |= (read_buffer[0] << 8);
                if (raw_data & (1 << 15))
                {
                    raw_data = ~raw_data;
                    raw_data >>= 4;
                    raw_data += 1;
                    raw_data = -raw_data;
                     
                } 
                else 
                {
                    raw_data >>= 4;
                }
                *read_data |= raw_data;
                read_success = true;
            }
            
        }

    return read_success;

}

bool get_accelerometer_z_axis_accel(int *fd, int16_t *read_data)
{
    bool read_success = false;
    *read_data = 0;

    uint8_t outbuf[1];
    uint8_t read_buffer[1];
    uint16_t raw_data = 0;
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = 0x19;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = 0x19;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;

    outbuf[0] = 0x2C;


        int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
        {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            raw_data = read_buffer[0];

            msgs[0].addr = 0x19;
            msgs[0].flags = 0;
            msgs[0].len = 1;
            msgs[0].buf = outbuf;

            msgs[1].addr = 0x19;
            msgs[1].flags = I2C_M_RD;
            msgs[1].len = 1;
            msgs[1].buf = read_buffer;

            msgset[0].msgs = msgs;
            msgset[0].nmsgs = 2;

            outbuf[0] = 0x2D;
            
            if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
            {
                perror("ioctl(I2C_RDWR) in i2c_read\n");
            } 
            else
            {
                raw_data |= (read_buffer[0] << 8);
                if (raw_data & (1 << 15))
                {
                    raw_data = ~raw_data;
                    raw_data >>= 4;
                    raw_data += 1;
                    raw_data = -raw_data;
                     
                } 
                else 
                {
                    raw_data >>= 4;
                }
                *read_data |= raw_data;
                read_success = true;
            }
            
        }

    return read_success;

}

bool get_accelerometer_data(int *fd, int16_t *x_data,  int16_t *y_data, int16_t *z_data )
{
    bool success = false;
    if(get_accelerometer_x_axis_accel(fd, x_data))
    {
        success = true;
    }
    success &= get_accelerometer_y_axis_accel(fd, y_data);
    success &= get_accelerometer_z_axis_accel(fd, z_data);

    return success;
}

bool read_CRA_REG_M (int *fd, uint8_t *read_buffer, size_t buffer_size)
{
    bool read_success = false;
    uint8_t reg_addr = 0x00;
    uint8_t magnometer_addr = 0x1e;

    uint8_t outbuf[1];
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = magnometer_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = magnometer_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;

    outbuf[0] = 0x00;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool write_CRA_REG_M(int *fd, uint8_t data)
{

    bool read_success = false;
    uint8_t reg = 0x00;
    uint8_t magnometer_addr = 0x1e;

    int retval;
    uint8_t outbuf[2];
    outbuf[0] = reg;
    outbuf[1] = data;
    struct i2c_msg msgs[3];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = magnometer_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2;
    msgs[0].buf = outbuf;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 1;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c write data\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool read_CRB_REG_M (int *fd, uint8_t *read_buffer, size_t buffer_size)
{
    bool read_success = false;
    uint8_t reg_addr = 0x01;
    uint8_t magnometer_addr = 0x1e;

    uint8_t outbuf[1];
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = magnometer_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = magnometer_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;

    outbuf[0] = reg_addr;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool write_CRB_REG_M(int *fd, uint8_t data)
{

    bool read_success = false;
    uint8_t reg_addr = 0x01;
    uint8_t magnometer_addr = 0x1e;

    int retval;
    uint8_t outbuf[2];
    outbuf[0] = reg_addr;
    outbuf[1] = data;
    struct i2c_msg msgs[3];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = magnometer_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2;
    msgs[0].buf = outbuf;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 1;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c write data\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool read_MR_REG_M (int *fd, uint8_t *read_buffer, size_t buffer_size)
{
    bool read_success = false;
    uint8_t reg_addr = 0x02;
    uint8_t magnometer_addr = 0x1e;

    uint8_t outbuf[1];
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = magnometer_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = magnometer_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;

    outbuf[0] = reg_addr;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool write_MR_REG_M(int *fd, uint8_t data)
{

    bool read_success = false;
    uint8_t reg_addr = 0x02;
    uint8_t magnometer_addr = 0x1e;

    int retval;
    uint8_t outbuf[2];
    outbuf[0] = reg_addr;
    outbuf[1] = data;
    struct i2c_msg msgs[3];
    struct i2c_rdwr_ioctl_data msgset[1];

    msgs[0].addr = magnometer_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2;
    msgs[0].buf = outbuf;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 1;


    int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) {
            perror("ioctl(I2C_RDWR) in i2c write data\n");
        } 
        else 
        {
            read_success = true;
        }

    return read_success;
}

bool get_magnometer_x_axis(int *fd, int16_t *read_data)
{
    bool read_success = false;
    const uint8_t magnometer_address = 0x1e;
    uint8_t OUT_X_H_addr = 0x03;
    uint8_t OUT_X_L_addr = 0x04;

    *read_data = 0;

    uint8_t outbuf[1];
    uint8_t read_buffer[1];
    uint8_t h_data = 0;
    uint8_t l_data = 0;
    uint16_t raw_data = 0;
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    outbuf[0] = OUT_X_H_addr;
    msgs[0].addr = magnometer_address;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = magnometer_address;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;




        int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
        {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            raw_data = read_buffer[0];

            outbuf[0] = OUT_X_L_addr;
            msgs[0].addr = magnometer_address;
            msgs[0].flags = 0;
            msgs[0].len = 1;
            msgs[0].buf = outbuf;

            msgs[1].addr = magnometer_address;
            msgs[1].flags = I2C_M_RD;
            msgs[1].len = 1;
            msgs[1].buf = read_buffer;

            msgset[0].msgs = msgs;
            msgset[0].nmsgs = 2;


            
            if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
            {
                perror("ioctl(I2C_RDWR) in i2c_read\n");
            } 
            else
            {
                raw_data = (read_buffer[0] | (raw_data << 8));
                *read_data |= raw_data;
                read_success = true;
            }
            
        }

    return read_success;

}

bool get_magnometer_z_axis(int *fd, int16_t *read_data)
{
    bool read_success = false;
    const uint8_t magnometer_address = 0x1e;
    uint8_t OUT_Y_H_addr = 0x05;
    uint8_t OUT_Y_L_addr = 0x06;

    *read_data = 0;

    uint8_t outbuf[1];
    uint8_t read_buffer[1];
    uint16_t raw_data = 0;
    uint8_t h_data = 0;
    uint8_t l_data = 0;
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    outbuf[0] = OUT_Y_H_addr;
    msgs[0].addr = magnometer_address;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = magnometer_address;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;




        int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
        {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            raw_data = read_buffer[0];

            outbuf[0] = OUT_Y_L_addr;
            msgs[0].addr = magnometer_address;
            msgs[0].flags = 0;
            msgs[0].len = 1;
            msgs[0].buf = outbuf;

            msgs[1].addr = magnometer_address;
            msgs[1].flags = I2C_M_RD;
            msgs[1].len = 1;
            msgs[1].buf = read_buffer;

            msgset[0].msgs = msgs;
            msgset[0].nmsgs = 2;


            
            if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
            {
                perror("ioctl(I2C_RDWR) in i2c_read\n");
            } 
            else
            {
                raw_data = (read_buffer[0] | (raw_data << 8));
                *read_data |= raw_data;
                read_success = true;
            }
            
        }

    return read_success;

}

bool get_magnometer_y_axis(int *fd, int16_t *read_data)
{
    bool read_success = false;
    const uint8_t magnometer_address = 0x1e;
    uint8_t OUT_Y_H_addr = 0x07;
    uint8_t OUT_Y_L_addr = 0x08;

    *read_data = 0;

    uint8_t outbuf[1];
    uint8_t read_buffer[1];
    uint16_t raw_data = 0;
    struct i2c_msg msgs[2];
    struct i2c_rdwr_ioctl_data msgset[1];

    outbuf[0] = OUT_Y_H_addr;
    msgs[0].addr = magnometer_address;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = outbuf;

    msgs[1].addr = magnometer_address;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = read_buffer;

    msgset[0].msgs = msgs;
    msgset[0].nmsgs = 2;




        int ret = 0;
        if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
        {
            perror("ioctl(I2C_RDWR) in i2c_read\n");
        } 
        else 
        {
            raw_data = read_buffer[0];

            outbuf[0] = OUT_Y_L_addr;
            msgs[0].addr = magnometer_address;
            msgs[0].flags = 0;
            msgs[0].len = 1;
            msgs[0].buf = outbuf;

            msgs[1].addr = magnometer_address;
            msgs[1].flags = I2C_M_RD;
            msgs[1].len = 1;
            msgs[1].buf = read_buffer;

            msgset[0].msgs = msgs;
            msgset[0].nmsgs = 2;


            
            if ( (ret = ioctl(*fd, I2C_RDWR, &msgset)) < 0) 
            {
                perror("ioctl(I2C_RDWR) in i2c_read\n");
            } 
            else
            {
                raw_data = (read_buffer[0] | (raw_data << 8));
                *read_data |= raw_data;
                read_success = true;
            }
            
        }

    return read_success;

}

bool get_magnometer_data(int *fd, int16_t *x_data,  int16_t *y_data, int16_t *z_data )
{
    bool success = false;
    if(get_magnometer_x_axis(fd, x_data))
    {
        success = true;
    }
    success &= get_magnometer_y_axis(fd, y_data);
    success &= get_magnometer_z_axis(fd, z_data);

    return success;
}