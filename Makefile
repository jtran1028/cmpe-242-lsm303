CC=gcc
CFLAGS=-I.
DEPS = lsm303.h
OBJ = lsm303.o get_lsm303_data.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

get_lsm303_data: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)