# CMPE 242 LSM303

These modules are built for the JETSON NANO 2GB Platform to interface with an LSM303DLHC.

NANO Header Pins must connect to the following to the LSM303DLHC:

    NANO_HEADER_PIN_3 => LSM303DLHC_SDA

    NANO_HEADER_PIN_5 => LSM303DLHC_SCL

**To run the get_lsm_data program to output accelerometer and magnometer data**

The Makefile contains the build configuration for the lsm303.h and lsm303.c and get_lsm_data.c.

1. Run make 
2. Run ./get_lsm_data
3. The data will be displayed every 1 second over a course of 50 seconds.

