
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "lsm303.h"
#include "math.h"
#define PI 3.14159265

int main()
{
    int fd;
    int adapter_nr = 1;
    char filename[20];

    snprintf(filename, 19, "/dev/i2c-%d", adapter_nr);
    fd = open(filename, O_RDWR);
    if (fd < 0)
    {
        perror("Error in opening i2c-2\n");
        exit(1);
    }
    uint8_t input_buffer;
    bool success;
    success = read_CTRL_REG_1A(&fd, &input_buffer, 1);
    
    if (!success)
    {
        close(fd);
        return -1;
    }
    printf("CTR_REG_1A register value:%u\n", input_buffer);
    
    success = write_CTRL_REG_1A(&fd, 0x77);
    if(!success)
    {
        close(fd);
        return -1;
    }

    success = read_CTRL_REG_1A(&fd, &input_buffer, 1);
    if (!success)
    {
        close(fd);
        return -1;
    }
    printf("CTR_REG_1A register value:%u\n", input_buffer);

 
    success = read_CRA_REG_M(&fd, &input_buffer, 1);
    if (!success)
    {
        close(fd);
        return -1;
    }
    printf("read_CRA_REG_M register value:%#02x\n", input_buffer);

    success = read_CRB_REG_M(&fd, &input_buffer, 1);
    if (!success)
    {
        close(fd);
        return -1;
    }
    printf("read_CRB_REG_M register value:%#02x\n", input_buffer);

    success = write_CRA_REG_M(&fd, 0x1c);
    if(!success)
    {
        close(fd);
        return -1;
    }

    success = read_CRA_REG_M(&fd, &input_buffer, 1);
    if (!success)
    {
        close(fd);
        return -1;
    }
    printf("read_CRA_REG_M register value:%#02x\n", input_buffer);

    success = read_MR_REG_M(&fd, &input_buffer, 1);
    if (!success)
    {
        close(fd);
        return -1;
    }  
    printf("read_MR_REG_M register value:%#02x\n", input_buffer);


    success = write_MR_REG_M(&fd, 0x00);
    if (!success)
    {
        close(fd);
        return -1;
    }  

    success = read_MR_REG_M(&fd, &input_buffer, 1);
    if (!success)
    {
        close(fd);
        return -1;
    }  
    printf("read_MR_REG_M register value:%#02x\n", input_buffer);
      
    int16_t x_axis_accel;
    int16_t y_axis_accel;
    int16_t z_axis_accel; 

    int16_t x_axis_mag;
    int16_t y_axis_mag;
    int16_t z_axis_mag;    

    const float XY_GAIN = 1100;
    const float Z_GAIN = 980;

    double heading;
    double val = 180.0 / PI;
    
    int cont = 1;
    while (cont < 50)
    {
        success = get_accelerometer_data(&fd, &x_axis_accel, &y_axis_accel, &z_axis_accel);
        if(!success)
        {
            printf("Failed to get acceleration data\n");
            close(fd);
            return -1;
        }

        success = get_magnometer_data(&fd, &x_axis_mag, &y_axis_mag, &z_axis_mag);
        if(!success)
        {
            printf("Failed to get acceleration data\n");
            close(fd);
            return -1;
        }                 
        printf("Accelerometer: X:%0.2fg, Y:%0.2fg, Z:%0.2fg\t", ((float)x_axis_accel / 1000), ((float)y_axis_accel/1000), ((float) z_axis_accel / 1000));
        printf("RAW Magnometer: X:%d, Y:%d, Z:%d \n", (x_axis_mag), (y_axis_mag), (z_axis_mag));     
        sleep(1);
        cont++;
    }


    close(fd);
    return 0;
}